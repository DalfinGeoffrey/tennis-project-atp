import axios from "axios";
import { mapGetters } from "vuex";

const API_URL = 'http://localhost:3000';

export default {
    //load All tournaments
    loadTournament() {
        return axios.get(`${API_URL}/getAllTournoi`);
    },
    getEquipe() {
        return axios.get(`${API_URL}/equipeById/${id}`);
    },
    //load All Standings
    loadRanking() {
        return axios.get(`${API_URL}/getAllJoueur`);
    },
    //load All meet
    loadMeetingofTournamentById(id) {
        return axios.get(`${API_URL}/matchByTournoi/${id}`);
    },
    loadMatch(id) {
        return axios.get(`${API_URL}/match/${id}`);
    },
    getEquipe1(id) {
        return axios.get(`${API_URL}/getPlayerByIdEquipe/${id}`);
    },
    getSetEquipe1(idMatch, idEquipe) {
        return axios.get(`${API_URL}/countSet/${idMatch}/${idEquipe}`);
    },
    getJeuEquipe1(idSet, idPlayer) {
        return axios.get(`${API_URL}/countJeuWithIdPlayer/${idSet}/${idPlayer}`);
    },
    getEquipe2(id) {
        return axios.get(`${API_URL}/getPlayerByIdEquipe/${id}`);
    },
    getSetEquipe2(idMatch, idEquipe) {
        return axios.get(`${API_URL}/countSet/${idMatch}/${idEquipe}`);
    },
    getJeuEquipe2(idSet, idPlayer) {
        return axios.get(`${API_URL}/countJeuWithIdPlayer/${idSet}/${idPlayer}`);
    },
    followPlayer(idUser, idPlayer){
        return axios.post(`${API_URL}/addUtilisateurFollowingJoueur`,{idUser: idUser, idPlayer: idPlayer})
    },
    getUserFollowingPlayer(idUser, idPlayer){
        return axios.get(`${API_URL}/getUtilisateurFollowingJoueur/${idUser}/${idPlayer}`);
    },
    removeUtilisateurFollowingJoueur(idUser, idPlayer){
        return axios.post(`${API_URL}/removeUtilisateurFollowingJoueur`, {idUser, idPlayer});
    },
    getScoreMatch(idMatch){
        return axios.get(`${API_URL}/getScoreMatch/${idMatch}`);
    }
}