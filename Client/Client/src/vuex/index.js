import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const initialState = {
    drawer: false,
    connected: false,
    user:  undefined,
    standing: []
}

const getters = {
    drawer: ({ drawer }) => drawer,
    connected: ({ connected }) => connected,
    user: ({ user }) => user,
    standing: ({ standing }) => standing,
}

export default new Vuex.Store({
    state: initialState,
    mutations,
    actions,
    getters
})