import axios from 'axios'
import API from '@/lib/API'

const toggleDrawer = ({ commit }) => {
    commit('TOGGLE_DRAWER')
}

const connect = ({ commit }, {email, password}) => {
    axios.get("http://localhost:3000/getUser/"+email+"/"+password).then(function(response){
        if(response.status == 404){
            console.log("fake mdp")
        }else{
            console.log("succes")
            commit('CONNECT', response.data)
        }
      });
}

const standing = ({ commit }) => {
    API.loadRanking()
          .then(response => {
            commit('STANDING', response.data)
        });
}

const actions = {
    toggleDrawer,
    connect,
    standing
  }
  
  export default actions
  