const TOGGLE_DRAWER = (state) => {
    state.drawer = !state.drawer
}

const CONNECT = (state, user) => {
    state.connected = true
    state.user = user
  }

const STANDING = (state, standing) => {
    state.standing = standing
}

const mutations = {
    TOGGLE_DRAWER,
    CONNECT,
    STANDING
}


export default mutations
