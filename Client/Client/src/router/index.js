import Vue from 'vue'
import VueRouter from 'vue-router'
import Classement from '@/components/Classement'
import Player from '@/components/Player'
import Home from '@/components/Home'
import Profile from '@/components/Profile'
import Tournois from '@/components/Tournois'
import Match from '@/components/Match'
import Meeting from '@/components/Meetings'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/Classement',
      name: 'Classement',
      component: Classement
    },
    {
      path: '/Player/:id',
      name: 'Player',
      component: Player
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/tournois',
      name: 'Tournois',
      component: Tournois
    },
    {
      path: '/match/:id',
      name: 'Match',
      component: Match
    },
    {
      path: '/matchs/:id',
      name: 'Meeting',
      component: Meeting
    },
  ]
})
