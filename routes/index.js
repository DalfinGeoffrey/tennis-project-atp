var mysql = require('mysql');
var express = require('express');
var router = express.Router();
var userCtrl = require('../controllers/utilisateurController');
var arbitreCtrl = require('../controllers/arbitreController');
var tournoiCtrl = require('../controllers/tournoiController');
var matchCtrl = require('../controllers/matchController');
var joueurCtrl = require('../controllers/joueurController.js');
var equipeCtrl = require('../controllers/equipeController.js');
var setCtrl = require('../controllers/setController.js');
var jeuCtrl = require('../controllers/jeuController.js');
var organisationCtrl = require('../controllers/organisationController.js');
var courtCtrl = require('../controllers/courtController.js');

/* ROUTES FOR MATCH */
router.route('/matchByTournoi/:id').get(matchCtrl.getAllByIdTounois);
router.route('/matchByJoueur/:id').get(matchCtrl.getAllByJoueur);
router.route('/match/:id').get(matchCtrl.getById);
router.route('/matchByArbitre/:id').get(matchCtrl.getAllByArbitre);
router.route('/getMatchByArbitre').get(arbitreCtrl.getAll);
router.route('/resetMatch/:idGame').get(matchCtrl.resetMatch);
router.route('/getMatchByJoueur').get(joueurCtrl.getMatch);
router.route('/getMatchBySet').get(setCtrl.getMatch);
router.route('/addMatch').post(matchCtrl.addMatch);
router.route('/updateDateFin').post(matchCtrl.updateDateFin);

/* ROUTES FOR ORGANISATION */
router.route('/getAllOrganisation').get(organisationCtrl.getAll);
router.route('/organisation').post(organisationCtrl.getByNameAndPassword);

/* ROUTES FOR ARBITRE */
router.route('/arbitre').post(arbitreCtrl.getByNameAndPassword);
router.route('/getAllArbitre').get(arbitreCtrl.getAll);
router.route('/arbitre/:id').get(arbitreCtrl.getById);
router.route('/addArbitre').post(arbitreCtrl.addArbitre);
router.route('/updateArbitre').post(arbitreCtrl.updateArbitre);
router.route('/getMatch&Arbitre').get(arbitreCtrl.getAll);


router.route('/userById/:id').get(userCtrl.getUserById);


/* ROUTES FOR INCIDENT */
router.route('/AddIncident').post(matchCtrl.addIncident);
router.route('/cloreIncident').post(matchCtrl.cloreIncident);

/* ROUTES FOR TOURNOI */
router.route('/getAllTournoi').get(tournoiCtrl.getAll);
router.route('/tournoi/:id').get(tournoiCtrl.getById);
router.route('/getAllByOrganisation/:id').get(tournoiCtrl.getByOrganisation);
router.route('/addTournoi').post(tournoiCtrl.addTournoi);
router.route('/updateTournoi').post(tournoiCtrl.updateTournoi);
router.route('/deleteTournoi/:id').get(tournoiCtrl.deleteTournoi);

/* ROUTES FOR COURT */
router.route('/getAllCourt').get(courtCtrl.getAll);
router.route('/matchByTournoi/:id').get(matchCtrl.getAllByIdTounois);
router.route('/matchByJoueur/:id').get(matchCtrl.getAllByJoueur);
router.route('/match/:id').get(matchCtrl.getById);
router.route('/matchByArbitre/:id').get(matchCtrl.getAllByArbitre);
router.route('/resetMatch/:idGame').get(matchCtrl.resetMatch);
router.route('/getScoreMatch/:idMatch').get(matchCtrl.getScoreMatch);


/* ROUTES FOR JEU */
router.route('/jeuById/:idJeu').get(jeuCtrl.getById);

/* ROUTES FOR USER */
router.route('/saveUser').post(userCtrl.addUser);
router.route('/getPublicById/:id').get(userCtrl.getPublicById);
router.route('/getPresseById/:id').get(userCtrl.getPublicById);
router.route('/postPresse').get(userCtrl.getPublicById);
router.route('/postPublic').get(userCtrl.getPublicById);
router.route('/getPlayerByIdEquipe/:id').get(joueurCtrl.getByIdEquipe);
router.route('/getUser/:email/:password').get(userCtrl.getUserByName);


/* ROUTES FOR JOUEURS */
router.route('/joueurs').get(joueurCtrl.getAll);
router.route('/joueur/:id').get(joueurCtrl.getById);
router.route('/getAllJoueur').get(joueurCtrl.getAll);
router.route('/addUtilisateurFollowingJoueur').post(joueurCtrl.addUtilisateurFollowingJoueur);
router.route('/getUtilisateurFollowingJoueur/:idUser/:idPlayer').get(joueurCtrl.getUtilisateurFollowingJoueur);
router.route('/removeUtilisateurFollowingJoueur').post(joueurCtrl.removeUtilisateurFollowingJoueur);

/* ROUTES FOR EQUIPE */
router.route('/equipeById/:id').get(equipeCtrl.getById);
router.route('/equipeByName/:team').get(equipeCtrl.getByName);
router.route('/addEquipe').post(equipeCtrl.addEquipe);

/* ROUTES FOR SET */
router.route('/setByMatch/:idMatch').get(setCtrl.getByIdMatch);
router.route('/set').get(setCtrl.addSet);
router.route('/addSet').post(setCtrl.addSet);
router.route('/addSetReel').post(setCtrl.addSetReel);
router.route('/updateSet').post(setCtrl.updateSet);
router.route('/getMatch&Set').get(setCtrl.getMatch);
router.route('/countSet/:id').get(setCtrl.countSet);
router.route('/countSet/:id/:id1').get(setCtrl.countSetWithIdPlayer);
router.route('/addMatch').post(matchCtrl.addMatch);

/* ROUTES FOR JEU */
router.route('/addJeu').post(jeuCtrl.addJeu);
router.route('/addJeuReel').post(jeuCtrl.addJeuReel);
router.route('/jeuxBySet/:idSet').get(jeuCtrl.getByIdSet);
router.route('/updateJeu').post(jeuCtrl.updateJeu);
router.route('/getMatchByJeu').get(jeuCtrl.getMatch);
router.route('/getMatch&Jeu').get(jeuCtrl.getMatch);
router.route('/countJeuWithIdPlayer/:idSet/:id1').get(jeuCtrl.countJeuWithIdPlayer);

/* ROUTES FOR POINT */
router.route('/addPoint').post(matchCtrl.addPoint);
router.route('/getMatchByPoint').get(matchCtrl.getMatch);

router.route("/getUtilisateursFollowJoueur/:id").get()
router.route("/addOrganisation").post(organisationCtrl.addOrganisation);

module.exports = router;
