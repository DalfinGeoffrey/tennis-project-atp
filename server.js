var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var routes = require('./routes/index.js');
var app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});
var db = require('./db.js');
//var port = process.env.PORT || 3000;
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function(req, res, next){
    res.io = io;
    next();
});
app.use(bodyParser.json());
app.use('', routes);

app.set('port', process.env.PORT || 3000);
//app.listen(port);

var server = app.listen(app.get('port'), function () {
    console.log("|-----| ATP-Project server in nodejs started |-----|");
});

var io = require('socket.io').listen(server);

io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});

module.exports = {io}
