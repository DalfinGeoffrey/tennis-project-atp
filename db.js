var mysql = require('mysql');
var express = require('express');


var db = mysql.createConnection({
    host        : '',
    // port        : '3308',
	user        : '',
	password    : '',
    database    : ''
});

db.connect((err)=> {
    if(err){
        console.log('|-----| not connected to database |-----| ');
    }else{
        console.log('|-----| connected to database |-----|');
    }
});

module.exports = db;
