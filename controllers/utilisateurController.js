var db = require('../db.js');
var datetime = require('node-datetime');

var crypto = require('../crypto.js');

module.exports = {
    addUser: function (req, res) {
        var dt = datetime.create();
        var nom = req.body.Nom;
        var prenom = req.body.Prenom;
        var email = req.body.Email;
        var password = req.body.Password;
        var passwordEncrypt = crypto.encrypt(password);
        var dateNaissance = req.body.DateNaissance;
        var phone = req.body.NumPortable;
        db.query('INSERT INTO utilisateur' +
            '(Nom, Prenom, Email, Password, DateNaissance, DateInscription,  NumPortable)' +
            'VALUES(?,?,?,?,?,?,?)',
            [nom,
            prenom,
            email,
            passwordEncrypt,
            dateNaissance,
            dt.format('Y-m-d H:M:S'),
            phone
            ],
            function (err, rows, fields) {
                if (err) {
                    console.log('pb création du user : ' + err);
                } else if(rows.count == 1){

                    console.log(rows);

                    res.contentType('application/json');
                    res.json(
                        rows.insertId
                    )
                    res.end();
                }
            });
    },


    getUserByName: function(req, res){
        var email = req.params.email;
        console.log(email);
        var password = crypto.encrypt(req.params.password);
        console.log(password);
        db.query('SELECT * FROM utilisateur WHERE Email=? AND Password=?',[req.params.email,password],
            function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch user' + rows);
                } else {
                    if(rows[0] == "undefined")
                    {
                        res.json({error : "not defined", status : 404});
                    }
                    res.contentType('application/json');
                    console.log(rows[0]);
                    res.json(
                        rows[0]
                    )
                    res.end();
                }
            });
    },

    //Get all database users, only usefull for test
    getUserById: function (req, res) {
        var id = req.params.id;
        db.query('SELECT * FROM utilisateur WHERE Id=' + id,
            function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch user' + rows);
                } else {
                    res.contentType('application/json');
                    res.json(
                        rows[0]
                    )
                    res.end();
                }
            });
        //do something
    },
    getPublicById: function (req, res) {
        var id = req.params.id;
        db.query('SELECT * FROM utilisateur INNER JOIN public ON IdPublic = Id WHERE IdPublic=' + id,
            function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch user' + rows);
                } else {
                    res.contentType('application/json');
                    res.json({
                        rows
                    })
                    res.end();
                }
            });
    },
    postUser: function (req, res) {

    }
}

