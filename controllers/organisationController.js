var db = require('../db.js');

module.exports = {
    getAll : function(req, res){
        var email = req.body.email;
        var password = req.body.password;
        db.query('SELECT * FROM organisation' , function(err, rows, fields){
            if(err){
                console.log('pb fetch orga' + rows);
            }else{
            res.contentType('application/json');

            res.json(
                rows
            )
            res.end();
        }
        });
    },
    getByNameAndPassword : function(req, res){
        var username = req.body.username;
        var password = req.body.password;
        db.query('SELECT * FROM `organisation` WHERE `UserName` = ? AND `Password`= ? LIMIT 1',[username, password], function(err, rows, fields){
            if(err){
                console.log('pb fetch arbitre' + rows);
            }else{
            res.contentType('application/json');

            res.json(
                rows[0]
            )
            res.end();
        }
        });
    },
    addOrganisation : function(req, res){
        db.query('INSERT INTO organisation(Nom, UserName, Password)'+
         'VALUES(?,?,?)',
         [req.body.nom,
          req.body.username, 
          req.body.password     
         ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
            res.contentType('application/json');
            res.json(
               rows.insertId
            )
            res.end();
        }
        });
    },

}