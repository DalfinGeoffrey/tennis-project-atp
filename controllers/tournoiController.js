var db = require('../db.js');

module.exports = {
    getAll : function(req, res){
        db.query('SELECT * FROM tournoi', function(err, rows, fields){
            if(err){
                console.log('pb fetch tournoi' + rows);
            }else{
            res.contentType('application/json');
            res.json(
               rows
            )
            res.end();
        }
        });
    },
    
    getById: function(req, res){
        db.query('SELECT * FROM tournoi WHERE IdTournoi='+req.params.id, function(err, rows, fields){
            if(err){
                console.log('pb fetch arbitre' + rows);
            }else{
            res.contentType('application/json');
            res.json(
               rows[0]
            )
            res.end();
        }
        });
    },
    getByOrganisation: function(req, res){
        db.query('SELECT * FROM tournoi WHERE IdOrganisation='+req.params.id, function(err, rows, fields){
            if(err){
                console.log('pb fetch arbitre' + rows);
            }else{
            res.contentType('application/json');

            res.json(
               rows
            )
            res.end();
        }
        });
    },
    addTournoi : function(req, res){
        db.query('INSERT INTO tournoi(IdOrganisation, Nom, NbSetGagnant, DateDebut, DateFin)'+
         'VALUES(?,?,?,?,?)',
         [req.body.idOrganisation,
          req.body.nom, 
          req.body.nbSet,
          req.body.dateDebut,
          req.body.dateFin
        ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
            res.contentType('application/json');
            res.json(
               rows.insertId
            )
            res.end();
        }
        });
    },
    updateTournoi : function(req, res){
        db.query('UPDATE tournoi SET IdOrganisation = ?, Nom = ?, NbSetGagnant = ?, DateDebut = ?, DateFin = ?'+
         'WHERE IdTournoi = ?',
         [req.body.idOrganisation,
          req.body.nom, 
          req.body.nbSet,
          req.body.dateDebut,
          req.body.dateFin,
          req.body.idTournoi
        ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
            res.contentType('application/json');
            res.json(
                req.body.idTournoi
            )
            res.end();
        }
        });
    },
    deleteTournoi : function(req, res){
        db.query('DELETE FROM tournoi WHERE IdTournoi = ?', [req.params.id] ,
        function(err, rows, fields){
        if(err){
            console.log(err);
        }else{
            res.contentType('application/json');
            res.json(
            req.body.idTournoi
            )
            res.end();
        }
        });
    },

    postTounoi : function(req, res){
       //do something
    }
}