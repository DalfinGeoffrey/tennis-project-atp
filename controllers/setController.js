var db = require('../db.js');

module.exports = {
    getByIdMatch: function (req, res) {
        var idMatch = req.params.idMatch
        console.log(idMatch);
        db.query('SELECT * FROM `set_` WHERE `IdMatch` = ? ', [idMatch], function (err, rows, fields) {
            if (err) {
                console.log('pb fetch set' + rows);
            } else {
                res.contentType('application/json');
                console.log(rows);
                res.json(
                    rows
                )
                res.end();
            }
        });
    },




    getMatch: function (req, res) {
        var idMatch = req.params.idMatch
        console.log(idMatch);
        db.query('SELECT * FROM set_ INNER JOIN matchs ON set_.IdMatch = matchs.IdMatch', function (err, rows, fields) {
            if (err) {
                console.log('pb fetch match with set' + rows);
            } else {
                res.contentType('application/json');
                console.log(rows);
                res.json(
                    rows
                )
                res.end();
            }
        });
    },
    addSet: function (req, res) {
        var idMatch = req.body.idMatch;
        console.log(idMatch);
        db.query('INSERT INTO `set_`(IdMatch) values(?) ', [idMatch], function (err, rows, fields) {
            if (err) {
                console.log('pb fetch add' + rows);
            } else {
                db.query('SELECT * FROM  `set_` WHERE `IdMatch` = ? ORDER BY IdSet DESC LIMIT 1', [idMatch], function (err, rows, fields) {
                    res.contentType('application/json');
                    res.json(
                        rows[0]
                    )
                    res.end();
                });
            }
        });
    },
    updateSet : function(req, res){
        var idEquipeGagnante = req.body.equipe;
        var set = req.body.set;
        var nbJeu = req.body.nbJeu;
        var dateval = req.body.dateval;
        db.query('UPDATE `set_` SET `IdEquipeGagnante` = ?, `NbJeu` = ?, DateSet = ? WHERE `IdSet` = ? '
        ,[
          idEquipeGagnante,
          nbJeu,
          dateval,
          set
        ], 
        function(err, rows, fields){
            if(err){
                console.log('pb fetch add' + rows);
            }
            else{
                res.end();
            }
        });
    },  
    addSetReel: function (req, res) {
        var idMatch = req.body.idMatch;
        db.query('INSERT INTO `set_`(IdMatch) ' +
            'values(?) ',
            [idMatch],

            function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch add' + rows);
                } else {
                    db.query('SELECT * FROM  `set_` WHERE `IdMatch` = ? ORDER BY IdSet DESC LIMIT 1', [idMatch], function (err, rows, fields) {
                        res.contentType('application/json');
                        res.json(
                            rows[0]
                        )
                        res.end();
                    });
                }
            });
    },

    

    countSet: function (req, res) {
        db.query('SELECT COUNT(*) FROM `set_` WHERE IdMatch ='+req.params.id, function (err, rows, fields) {
            if (err) {
                console.log('pb fetch add' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows[0]
                )
                res.end();
            }
        });

    },
        countSetWithIdPlayer: function (req, res) {
            var idMatch = req.params.id;
            console.log(idMatch);
            var idPlayer = req.params.id1;
            console.log(idPlayer);
            db.query('SELECT COUNT(*) FROM `set_` INNER JOIN `matchs` ON set_.IdMatch = matchs.IdMatch WHERE matchs.IdMatch='+idMatch+' AND matchs.Equipe1 ='+idPlayer+' OR matchs.Equipe2 ='+idPlayer, function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch add' + rows);
                } else {
                    res.contentType('application/json');
                    res.json(
                        rows[0]
                    )
                    res.end();
                }
            });
    },
}