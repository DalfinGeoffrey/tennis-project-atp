var db = require('../db.js');

module.exports = {

    addJeu: function (req, res) {
        var idSet = req.body.idSet;
        var equipeAuService = req.body.equipeAuService;
        var position = req.body.equipePosition;
        var tieBreak = req.body.tieBreak;

        db.query('INSERT INTO `jeu`(IdSet, EquipeAuService, Position, TieBreak) values(?,?,?,?) ',
            [
                idSet,
                equipeAuService,
                position,
                tieBreak
            ]
            ,
            function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch add' + rows);
                } else {
                    db.query('SELECT * FROM  `jeu` WHERE `IdSet` = ? ORDER BY IdSet DESC LIMIT 1', [idSet], function (err, rows, fields) {
                        res.contentType('application/json');
                        res.json(
                            rows[0]
                        )
                        res.end();
                    });
                }
            });
    },
    addJeuReel: function (req, res) {
        var idSet = req.body.idSet;
        var position = req.body.equipePosition;
        var tieBreak = req.body.tieBreak;

        db.query('INSERT INTO `jeu`(IdSet, DateJeu, Position, TieBreak) values(?, ?, ?,?) '
        ,
        [
          idSet, 
          req.body.dateval,
          position, 
          tieBreak
        ], 
        function(err, rows, fields){
            if(err){
                console.log('pb fetch add' + rows);
            }else{
                db.query('SELECT * FROM  `jeu` WHERE `IdJeu` = ? ORDER BY IdSet DESC LIMIT 1',[rows.insertId], function(err, rows, fields){
                    res.contentType('application/json');
                    res.json(
                        rows[0]
                    )
                    res.end();
                });
            }
        });
    },   
    updateJeu : function(req, res){
        db.query('INSERT INTO `jeu`(IdSet, DateJeu, Position) values(?, ?, ?) '
            ,
            [
                idSet,
                req.body.dateval,
                position
            ],
            function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch add' + rows);
                } else {
                    db.query('SELECT * FROM  `jeu` WHERE `IdJeu` = ? ORDER BY IdSet DESC LIMIT 1', [rows.insertId], function (err, rows, fields) {
                        res.contentType('application/json');
                        res.json(
                            rows[0]
                        )
                        res.end();
                    });
                }
            });
    },
    updateJeu: function (req, res) {
        var idEquipeGagnante = req.body.equipe;
        var jeu = req.body.jeu;
        var dateval = req.body.dateval;
        db.query('UPDATE `jeu` SET IdEquipeGagnante = ?, DateJeu = ?  WHERE IdJeu = ? '
            , [
                idEquipeGagnante,
                dateval,
                jeu
            ],
            function (err, rows, fields) {
                if (err) {
                    console.log('pb fetch add' + rows);
                }
                else {
                    res.json(jeu);
                    res.end();
                }
            });
    },
    getByIdSet: function (req, res) {
        var idSet = req.params.idSet

        db.query('SELECT * FROM `jeu` WHERE `IdSet` = ? ', [idSet], function (err, rows, fields) {
            if (err) {
                console.log('pb fetch set' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows
                )
                res.end();
            }
        });
    },
    getById: function (req, res) {
        var idJeu = req.params.idJeu

        db.query('SELECT * FROM `jeu` WHERE `IdJeu` = ? ', [idJeu], function (err, rows, fields) {
            if (err) {
                console.log('pb fetch set' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    },

    getMatch: function (req, res) {
        db.query('SELECT * FROM jeu INNER JOIN set_  ON jeu.IdSet = set_.IdSet INNER JOIN matchs ON set_.IdMatch = matchs.IdMatch', function (err, rows, fields) {
            if (err) {
                console.log('pb fetch match with jeu' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    },
    countJeuWithIdPlayer: function (req, res) {
        var idSet = req.params.idSet;
        var idPlayer = req.params.id1;
        db.query('SELECT COUNT(*) FROM `jeu` WHERE `IdSet` = '+idSet+' AND `IdEquipeGagnante` = '+idPlayer, function (err, rows, fields) {
            if (err) {
                console.log('pb fetch add' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    },
}