var db = require('../db.js');
var io = require('../server').io;


module.exports = {
    getAllByIdTounois: function (req, res) {
        console.log(req.params.id);
        db.query('SELECT matchs.*, eq1.Nom nomEquipe1, eq2.Nom nomEquipe2 FROM matchs' +
            ' INNER JOIN equipe eq1 ON eq1.IdEquipe = matchs.Equipe1' +
            ' INNER JOIN equipe eq2 ON eq2.IdEquipe = matchs.Equipe2' +
            ' WHERE IdTournoi =' + req.params.id, function (err, rows, fields) {
                if (err) {
                    console.log('tournamentById : ' + err);
                } else {
                    res.contentType('application/json');
                    res.json(
                        rows
                    )
                    res.end();
                }
            });
    },

    getAllByJoueur: function (req, res) {
        db.query('SELECT matchs.IdMatch, matchs.IdOrganisation, matchs.IdArbitre, matchs.IdCourt, matchs.IdTournoi, matchs.DateDebut, matchs.DateFin, matchs.IdJoueurAuService, matchs.NbSetGagnant  FROM matchs' +
            'INNER JOIN equipe ON matchs.IdMatch = equipe.IdMatch' +
            'INNER JOIN joueur ON equipe.IdEquipe = joueur.IdEquipe AND IdJoueur =' + req.params.Id, function (err, rows, fields) {
                if (err) {
                    console.log('tournamentByPlayer : ' + rows);
                } else {
                    res.contentType('application/json');
                    res.json({
                        rows
                    })
                    res.end();
                }
            });
    },
    getAllByArbitre: function (req, res) {
        db.query('SELECT * FROM matchs WHERE IdArbitre = ' + req.params.id, function (err, rows, fields) {
            if (err) {
                console.log('matchByArbitre : ' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows
                )
                res.end();
            }
        });
    },

    getById: function (req, res) {
        db.query('SELECT * FROM matchs WHERE IdMatch=' + req.params.id, function (err, rows, fields) {
            if (err) {
                console.log(req.params.id)
                console.log('tournamentById : ' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    },
    addPoint: function (req, res) {
        db.query('INSERT INTO points(IdJeu, Point, Avantage, IdEquipe, DatePoint)' +
            'VALUES(?,?,?,?,?)',
            [req.body.idJeu,
            req.body.point,
            req.body.avantage,
            req.body.equipe,
            req.body.dateval
            ]
            ,
            function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    console.log(req.params.id)
                    console.log('tournamentById : ' + rows);
                } else {
                    console.log(rows.insertId);
                    db.query('SELECT * FROM  `points` WHERE `IdPoints` = ?  LIMIT 1', [rows.insertId], function (err, rows, fields) {
                        res.io.emit('Add', rows[0]);
                        res.json(
                            rows[0]
                        )
                        res.end();
                    });

                }
            });
    },

    getMatch: function (req, res) {
        db.query('SELECT * FROM points INNER JOIN jeu ON points.IdJeu = jeu.IdJeu INNER JOIN set_  ON jeu.IdSet = set_.IdSet INNER JOIN matchs ON set_.IdMatch = matchs.IdMatch', function (err, rows, fields) {
            if (err) {
                console.log('match with point : ' + rows);
            } else {
                res.contentType('application/json');
                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    },
    addMatch: function (req, res) {
        db.query('INSERT INTO matchs' +
            '(Equipe1, Equipe2, IdArbitre, IdCourt, IdOrganisation, IdTournoi,  NbSetGagnant)' +
            'VALUES(?,?,?,?,?,?,?)',
            [req.body.equipe1,
            req.body.equipe2,
            req.body.arbitre,
            req.body.court,
            req.body.organisation,
            req.body.tournoi,
            req.body.nbSet
            ]
            ,
            function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    console.log(req.params.id)
                    console.log('tournamentById : ' + rows);
                } else {
                    console.log(rows);
                    res.contentType('application/json');
                    res.json(
                        rows.insertId
                    )
                    res.end();
                }
            });
    },
    resetMatch: function (req, res) {
        db.query('DELETE FROM set_ where IdMatch = ?'
            ,
            [
                req.params.idGame
            ]
            ,
            function (err, rows, fields) {
                if (err) {
                    console.log(err);
                } else {
                    res.end();
                }
            });
    },
    updateDateFin : function(req, res){
        db.query('Update matchs SET DateFin = ? WHERE IdMatch = ?'
        ,
         [
            req.body.dateFin,
            req.body.idMatch
        ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
                res.json(
                    rows.insertId
                 )
            res.end();
        }
        });
    },
    addIncident : function(req, res){
        db.query('INSERT INTO incident(IdMatch, DateIncident, LibelleIncident, NiveauIncident) VALUES(?,?,?,?)'
        ,
         [
            req.body.idMatch,
            req.body.dateIncident,
            req.body.incident,
            req.body.incidentNiveau
        ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
                res.json(
                    rows.insertId
                 )
            res.end();
        }
        });
    },
    cloreIncident : function(req, res){
        console.log(req.body.dateIncident);
        db.query('UPDATE incident Set DateFinIncident = ?'+
        'WHERE IdIncident = ?'
        ,
         [
            req.body.dateIncident,
            req.body.idIncident
        ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
                console.log("ok");
                res.json(
                    req.body.idIncident
                 )
            res.end();
        }
        });
    },
    
    getScoreMatch: function (req, res) {
        console.log(req.params.idMatch);
        db.query('SELECT jeu.DateJeu, set_.DateSet, points.DatePoint, equipe.Nom, points.Point, jeu.EquipeAuService, jeu.Position FROM matchs' +
            ' INNER JOIN set_ ON matchs.IdMatch = set_.IdMatch' +
            ' INNER JOIN jeu ON set_.IdSet = jeu.IdSet' +
            ' INNER JOIN points ON jeu.IdJeu = points.IdJeu' +
            ' INNER JOIN equipe ON points.IdEquipe= equipe.IdEquipe' +
            ' Where matchs.IdMatch =' + req.params.idMatch, function (err, rows, fields) {
                if (err) {
                    console.log('tournamentById : ' + err);
                } else {
                    res.contentType('application/json');
                    console.log(rows[0]);
                    res.json(
                        rows
                    )
                    res.end();
                }
            });
    },
 };

