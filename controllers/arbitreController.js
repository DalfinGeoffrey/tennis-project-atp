var db = require('../db.js');

module.exports = {
    getByNameAndPassword : function(req, res){
        var email = req.body.email;
        var password = req.body.password;
        db.query('SELECT * FROM `arbitre` WHERE `Email` = ? AND `Password`= ? LIMIT 1',[email, password], function(err, rows, fields){
            if(err){
                console.log(email)
                console.log('pb fetch arbitre' + rows);
            }else{
            res.contentType('application/json');
            console.log(email)

            res.json(
                rows[0]
            )
            res.end();
        }
        });
    },
    getAll : function(req, res){
        db.query('SELECT * FROM `arbitre`', function(err, rows, fields){
            if(err){
                console.log('pb fetch arbitre' + rows);
            }else{
            res.contentType('application/json');

            res.json(
                rows
            )
            res.end();
        }
        });
    },
    getById : function(req, res){
        db.query('SELECT * FROM `arbitre` WHERE `Id`='+req.params.id+' LIMIT 1', function(err, rows, fields){
            if(err){
                console.log('pb fetch arbitre' + rows);
            }else{
            res.contentType('application/json');
            res.json(
               rows[0]
            )
            res.end();
        }
        });
    },

    getMatch : function(req, res){
        db.query('SELECT * FROM arbitre INNER JOIN matchs ON arbitre.Id = matchs.IdArbitre', function(err, rows, fields){
            if(err){
                console.log('pb fetch match with arbitre' + rows);
            }else{
            res.contentType('application/json');
            res.json(
               rows[0]
            )
            res.end();
        }
        });
    },
    addArbitre : function(req, res){
        db.query('INSERT INTO arbitre(Nom, Prenom, Email, Password)'+
         'VALUES(?,?,?,?)',
         [req.body.nom,
          req.body.prenom, 
          req.body.email,
          req.body.password     
         ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
            res.contentType('application/json');
            res.json(
               rows.insertId
            )
            res.end();
        }
        });
    },
    updateArbitre : function(req, res){
        db.query('UPDATE arbitre SET Nom = ?, Prenom = ?, Email = ?, Password = ?'+
         'WHERE Id = ?',
         [req.body.nom,
          req.body.prenom, 
          req.body.email,
          req.body.password,
          req.body.idArbitre,
        ]
         ,
          function(err, rows, fields){
            if(err){
                console.log(err);
            }else{
            res.contentType('application/json');
            res.json(
                req.body.idArbitre
            )
            res.end();
        }
        });
    },
    postArbitre : function(req, res){
       //do something
    }

    
}