var db = require('../db.js');

module.exports = {
    getAll: function (req, res) {
        db.query('SELECT * FROM joueur', function (err, rows, fields) {
            if (err) {

                console.log('pb fetch joueur' + rows);
            } else {
                res.contentType('application/json');

                res.json(
                    rows
                )
                res.end();
            }
        });
    },

    addUtilisateurFollowingJoueur: function (req, res) {
        console.log("toto")
        db.query('INSERT INTO suitjoueur(IdPublic, IdJoueur)' +
            'VALUES(?,?)',
            [req.body.idUser,
            req.body.idPlayer
            ],
            function (err, rows, fields) {
                if (err) {

                } else {
                    res.contentType('application/json');
                    res.sendStatus(200)
                    res.end();
                }
            });
    },
    getUtilisateurFollowingJoueur: function (req, res) {
        console.log(req.params.idUser);
        console.log(req.params.idPlayer);
        db.query('SELECT * FROM suitjoueur Where IdPublic='+req.params.idUser+' AND IdJoueur='+req.params.idPlayer, function (err, rows, fields) {
            if (err) {

            } else {
                res.contentType('application/json');
                console.log(rows[0]);
                if (rows[0] !== undefined) {
                    res.sendStatus(200)
                    res.end();
                }

            }
        });
    },

    removeUtilisateurFollowingJoueur: function (req, res) {
        console.log(req.body.idUser);
        console.log(req.body.idPlayer);
        db.query('DELETE FROM suitjoueur WHERE IdPublic='+req.body.idUser+' AND IdJoueur='+req.body.idPlayer, function (err, rows, fields) {
            if (err) {

            } else {
                res.contentType('application/json');
                console.log(rows[0]);
                if (rows[0] === undefined) {
                    res.sendStatus(200)
                    res.end();
                }

            }
        });
    },


    getById: function (req, res) {
        db.query('SELECT * FROM `joueur` WHERE IdJoueur=' + req.params.id, function (err, rows, fields) {
            if (err) {

                console.log('pb fetch joueur' + rows);
            } else {
                res.contentType('application/json');

                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    },

    getByIdEquipe: function (req, res) {
        db.query('SELECT DISTINCT joueur.IdJoueur, joueur.Nom, joueur.Photo, joueur.Classement FROM `equipejoueur` INNER JOIN joueur ON equipejoueur.IdJoueur = joueur.IdJoueur  WHERE equipejoueur.IdEquipe=' + req.params.id, function (err, rows, fields) {
            if (err) {

                console.log('pb fetch joueur' + rows);
            } else {
                res.contentType('application/json');

                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    },



    getMatch: function (req, res) {
        db.query('SELECT * FROM ((joueur INNER JOIN equipejoueur ON joueur.IdJoueur = equipejoueur.IdJoueur) INNER JOIN matchs ON equipejoueur.IdEquipe = matchs.Equipe1 OR equipejoueur.IdEquipe = matchs.Equipe2);', function (err, rows, fields) {
            if (err) {

                console.log('pb fetch matchs - ' + rows);
            } else {
                res.contentType('application/json');

                res.json(
                    rows[0]
                )
                res.end();
            }
        });
    }
}