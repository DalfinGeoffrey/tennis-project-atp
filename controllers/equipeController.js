var db = require('../db.js');

module.exports = {

    getById : function(req, res){
        db.query('SELECT * FROM `equipe` WHERE `IdEquipe`='+req.params.id, function(err, rows, fields){
            if(err){
                console.log(req.params.id);
                console.log('equipe : ' + rows);
            }else{
            res.contentType('application/json');
            res.json(
                rows[0]
             )
            res.end();
        }
        });
    },
    addEquipe : function(req, res){
        db.query('INSERT INTO `equipe`(Nom) values(?) '
        ,[
          req.body.nom
        ], 
        function(err, rows, fields){
            if(err){
                console.log('pb fetch add' + rows);
            }else{
                res.json(
                    rows.insertId
                );
                db.query('INSERT INTO `equipejoueur`(IdJoueur, IdEquipe) values(?, LAST_INSERT_ID())',[req.body.joueur], function(err, rows, fields){
                    res.end();
                });
            }
        });
    },   
    getByName : function(req, res){
        var team = req.params.team;

        db.query('SELECT * FROM `equipe` WHERE `Nom`= ?',[team], function(err, rows, fields){
            if(err){
                console.log('equipe : ' + rows);
            }else{
            res.contentType('application/json');
            res.json(
               rows[0]
            )
            res.end();
        }
        });
    }
    
 };